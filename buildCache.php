<?php
define('NineteenEleven', TRUE);
require_once	"config/config.php";
set_time_limit(128);

$files = glob('cache/*'); // get all file names
foreach($files as $file){ // iterate files
  if(is_file($file)){
    unlink($file); // delete file
	}
}
$startTime=date('U');
$mysqli = new mysqli(DB_HOST, DB_USER, DB_PASS, SB_DB) or die($mysqli->connect_error);
$SteamID = new SteamID;
$SteamQuery = new SteamQuery;

$lookupSQL = "SELECT * FROM `sb_admins` WHERE srv_group != 'Donor' ORDER BY `aid` ";

$result = $mysqli->query($lookupSQL) or die($mysqli->error . $mysqli->errno);

$id64list = null;

while($row = $result->fetch_array(MYSQLI_ASSOC)){

	$id64 = $SteamID->IDto64($row['authid']);

	$id64list .= $id64 . ",";
}
$id64list = rtrim($id64list, ",");

$Query = $SteamQuery->GetPlayerSummaries($id64list);

foreach ($Query->response->players as $player) {
	$recentGames = @$SteamQuery->GetRecentlyPlayedGames($player->steamid);
}
$endTime=date('U');
$completionTime = $endTime - $startTime;

echo "Files Deleted and Files Cached " . date('n/j/y \@ g:i:s a'). ", took {$completionTime} seconds.";
