<?php
///https://developer.valvesoftware.com/wiki/Steam_Web_API
if(!defined('NineteenEleven')){die('Direct access not premitted');}
define("DB_HOST" , "localhost");      //set MySQL host
define("DB_USER" , "root");         //MySQL username
define("DB_PASS" , "pass");       //MySQL password

define("SB_DB" , "sourcebans");          //donations database
define("API_KEY" , "xxxxxxxxxxxxxxxxxxxxxxxx");


//76561198009658881,76561198000911764,76561198058831315,76561198065521132,76561198027999846,76561198040056175,76561198036794478
//http://api.steampowered.com/ISteamUser/GetPlayerSummaries/v0002/?key=1D1303982FE9A16E0AE142F0DF2CE58F&format=xml&steamids=76561198009658881
//http://api.steampowered.com/IPlayerService/GetRecentlyPlayedGames/v0001/?key=1D1303982FE9A16E0AE142F0DF2CE58F&format=json&steamid=76561198009658881

function convertToHoursMins($time, $format = '%d:%d') {
    settype($time, 'integer');
    if ($time < 1) {
        return;
    }
    $hours = floor($time/60);
    $minutes = $time%60;
    return sprintf($format, $hours, $minutes);
}

 

class SteamQuery
{
    public function getJson($url) {
        // cache files are created like cache/abcdef123456...
        $cacheFile = 'cache' . DIRECTORY_SEPARATOR . md5($url);

        if (file_exists($cacheFile)) {
            $fh = fopen($cacheFile, 'r');
            $cacheTime = trim(fgets($fh));

            // if data was cached recently, return cached data
            if ($cacheTime > strtotime('-15 minutes')) {
                return fread($fh, filesize($cacheFile));
            }

            // else delete cache file
            fclose($fh);
            unlink($cacheFile);
        }

        $json = file_get_contents($url);

        $fh = fopen($cacheFile, 'w');
        fwrite($fh, time() . "\n");
        fwrite($fh, $json);
        fclose($fh);

        return $json;
    }


    public function GetPlayerSummaries($steamID64){
        $API_link = "http://api.steampowered.com/ISteamUser/GetPlayerSummaries/v0002/?key=" . API_KEY . "&format=json&steamids=" . $steamID64;
        $json = $this->getJson($API_link);
        $json_output=json_decode($json);
        return $json_output;
    }

    public function GetFriendsList($steamID64){
        $API_link = "http://api.steampowered.com/ISteamUser/GetFriendList/v0001/?key=". API_KEY ."&steamid=". $steamID64 . "&relationship=friend&format=json";
        $json = $this->getJson($API_link);
        $json_output=json_decode($json);
        return $json_output;
    }
    public function GetPlayerAchievements($steamID64,$appid){
        $API_link = "http://api.steampowered.com/ISteamUserStats/GetPlayerAchievements/v0001/?appid=". $appid ."&key=" . API_KEY . "&steamid=" . $steamID64 ."&format=json";
        $json = $this->getJson($API_link);
        $json_output=json_decode($json);
        return $json_output;
    }
    public function GetUserStatsForGame($steamID64,$appid){
        $API_link = "http://api.steampowered.com/ISteamUserStats/GetUserStatsForGame/v0002/?appid=". $appid ."&key=" . API_KEY . "&steamid=" . $steamID64 ."&format=json";
        $json = $this->getJson($API_link);
        $json_output=json_decode($json);
        return $json_output;
    }
    public function GetOwnedGames($steamID64){
        $API_link = "http://api.steampowered.com/IPlayerService/GetOwnedGames/v0001/?key=" . API_KEY . "&format=json&steamid=" . $steamID64;
        $json = $this->getJson($API_link);
        $json_output=json_decode($json);
        return $json_output;
    }
    public function GetRecentlyPlayedGames($steamID64){
        $API_link = "http://api.steampowered.com/IPlayerService/GetRecentlyPlayedGames/v0001/?key=" . API_KEY . "&format=json&steamid=" . $steamID64;
        $json = $this->getJson($API_link);
        $json_output=json_decode($json);
        return $json_output;
    }
}







class SteamID
{
    function IDto64($steamId) {
        $iServer = "0";
        $iAuthID = "0";
         
        $szTmp = strtok($steamId, ":");
         
        while(($szTmp = strtok(":")) !== false)
        {
            $szTmp2 = strtok(":");
            if($szTmp2 !== false)
            {
                $iServer = $szTmp;
                $iAuthID = $szTmp2;
            }
        }
        if($iAuthID == "0")
            return "0";
     
        $steamId64 = bcmul($iAuthID, "2");
        $steamId64 = bcadd($steamId64, bcadd("76561197960265728", $iServer));
            if (strpos($steamId64, ".")) {
                $steamId64=strstr($steamId64,'.', true);
            }     
        return $steamId64;
    }
    
    //convertSteamId64ToStamId converts 76561197973578969 to STEAM_0:1:6656620.
    function IDfrom64($steamId64) {
        $iServer = "1";
        if(bcmod($steamId64, "2") == "0") {
            $iServer = "0";
        }
        $steamId64 = bcsub($steamId64,$iServer);
        if(bccomp("76561197960265728",$steamId64) == -1) {
            $steamId64 = bcsub($steamId64,"76561197960265728");
        }
        $steamId64 = bcdiv($steamId64, "2");
        if (strpos($steamId64, ".")) {
                $steamId64=strstr($steamId64,'.', true);
            }     
        return ("STEAM_0:" . $iServer . ":" . $steamId64);
    }

    function getSteamLink($steamId64){
        return "http://steamcommunity.com/profiles/".$steamId64;
    }

    function getSteam64Xml($steam_link_xml){
        $xml = @simplexml_load_file($steam_link_xml)
        or dun_fucked_up();;
        if(!empty($xml)) {
            $steamID64 = $xml->steamID64;
        }
        return $steamID64;
    }

    function steamIDCheck($steamiduser){
        $steamcommunity = "http://steamcommunity.com";
        //Look for STEAM_0:0:123456 variation
        if(preg_match("/^STEAM_/i", $steamiduser)){
            $steamId64= $this->IDto64($steamiduser);
            $steam_link = $this->getSteamLink($steamId64);
            $steam_id = strtoupper($steamiduser);
            $steamArray = array('steamid'=>$steam_id, 'steamID64' =>$steamId64, 'steam_link'=>$steam_link);
            return $steamArray;
        }else{
            if (preg_match("/^[a-z]/i", $steamiduser)) {
                if (preg_match("/(steamcommunity.com)+/i",$steamiduser)) {
                    if (preg_match("/(\/profiles\/)+/i", $steamiduser)) {
                        $i = preg_split("/\//i", $steamiduser);
                        $size = count($i) - 1;
                        $steamID64 = $i[$size];
                        $steam_link = $this->getSteamLink($steamID64);
                        $steam_id=$this->IDfrom64($steamID64);
                        $steamArray = array('steamid'=>$steam_id, 'steamID64' =>$steamID64, 'steam_link'=>$steam_link);
                        return $steamArray;

                    } elseif (preg_match("/(\/id\/)+/i",$steamiduser)) {
                        $i = preg_split("/\//i", $steamiduser);
                        $size = count($i) - 1;
                        $steam_link_xml = $steamcommunity . "/id/" . $i[$size] . "/?xml=1";
                        $steamID64 = $this->getSteam64Xml($steam_link_xml);
                        $steam_link = $this->getSteamLink($steamID64);
                        $steam_id=$this->IDfrom64($steamID64);
                        $steamArray = array('steamid'=>$steam_id, 'steamID64' =>$steamID64, 'steam_link'=>$steam_link);
                        return $steamArray;

                    } else {
                        return false;
                    }
                }else{
                    $steam_link_xml = $steamcommunity . "/id/" . $steamiduser . "/?xml=1";
                    $steamID64 = $this->getSteam64Xml($steam_link_xml);
                    $steam_link = $this->getSteamLink($steamID64);
                    $steam_id=$this->IDfrom64($steamID64);
                        if ($steam_id=="STEAM_0:0:0") {
                            return false;
                        }else{
                        $steamArray = array('steamid'=>$steam_id, 'steamID64' =>$steamID64, 'steam_link'=>$steam_link);
                        return $steamArray;
                        }

                }
            }else{
                return false;
            }
        }
    }
}

?>